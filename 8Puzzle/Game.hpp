#pragma once

#include "World.hpp"
#include <SFML/Graphics/RenderWindow.hpp>


class Game : sf::NonCopyable
{
public:
	explicit Game(const std::string& title, unsigned width, unsigned height);

	void run();


private:
	void processEvents();
	void render();


private:
	sf::RenderWindow _window;
	World _world;
	std::string _title;
	bool _fullScreen;
};