#include "State.hpp"
#include "Puzzle.hpp"

#include <SFML/Graphics/RenderTarget.hpp>

//State::State()
//{
//
//}

State::State(std::vector<int> board, int distance, State* parent)
{
	_distance = distance;
	_parent = parent;

	// populate tiles for state
	for (auto const& value : board) {
		_indexes.push_back(value);
	}
}

State::State(std::vector<Tile> board, int distance, State* parent)
{
	_distance = distance;
	_parent = parent;

	// populate tiles for state
	for (auto const& value : board) {
		_gameboard.push_back(value);
		_indexes.push_back(value.getIndex());
	}
}

State::State(std::vector<int> board, State* initial, int distance, State* parent)
{
	_distance = distance;
	_parent = parent;

	// populate tiles for state

	for (auto const& value : board) {
		_indexes.push_back(value);
	}
}

State::State(std::vector<Tile> board, State* initial, int distance, State* parent)
{
	_distance = distance;
	_parent = parent;

	// populate tiles for state
	for (auto const& value : board) {
		_gameboard.push_back(value);
		_indexes.push_back(value.getIndex());
	}
}

std::vector<Tile> State::getTilesbyIndexes(State* initial)
{
	_gameboard.clear();
	for (auto const& value : _indexes) {
		_gameboard.push_back(initial->_gameboard[value]);
	}
	return _gameboard;
}

State::~State()
{
}

// swap two tiles
void State::Swap(int i, int j)
{
	auto temp = _indexes[i];
	_indexes[i] = _indexes[j];
	_indexes[j] = temp;
}

// get a string representation of the state
std::string State::Value() // think this check is slowing the game down
{							// http://stackoverflow.com/questions/14120346/c-fastest-method-to-check-if-all-array-elements-are-equal
	std::string output = "";
	for (auto const& value : _indexes) {
		output += std::to_string(value);
		output += ",";
	}
	return output;
}
