#include "Game.hpp"

int main()
{
	// set the title
	std::string title = "15 Puzzle";

	// set the window size
	auto width = 480u;
	auto height = 600u;

	// create game instance
	Game game(title, width, height);

	// enter the run state
	game.run();

	// exit
	return 0;
}