#pragma warning(disable:4996)
#include "Puzzle.hpp"
#include "Utility.hpp"

#include <SFML/Graphics/RenderTarget.hpp>

#include <functional>
#include <algorithm>
#include <cassert>
#include <stdlib.h>
#include <map>

#include <queue>
#include <deque>
#include<set>

using namespace utility;


Puzzle::Puzzle()
	: _textures()
	, _tiles()
	, _gameTypeSelected(GameType::fifteen)
{
	// construct the board
	constructBoard();

	// store the inital state (win state)
	_initialState = new State(_tiles, 0, nullptr) ;

	// shuffle the board
	if (!shuffled)
	shuffle(_gameTypeSelected);

	// store the current state after the shuffle
	_currentState = new State(_tiles, 0, nullptr);
}

// clean up
Puzzle::~Puzzle()
{
	delete _initialState;
	delete _currentState;	
}

// contructs the game board
void Puzzle::constructBoard()
{
	_win = false;
	// calculate size variables based on game type
	unsigned int gridSize = (_gameTypeSelected == GameType::eight) ? 3u : 4u;
	unsigned int maxTileCount = (_gameTypeSelected == GameType::eight) ? 9u : 16u;
	
	// create texture path
	std::string path = (_gameTypeSelected == GameType::eight) ? std::string("Media/Tiles/eight/") : std::string("Media/Tiles/fifteen/");

	// clear the current tile storage
	_tiles.clear();

	// allocate the tile storage based on the game type
	_tiles.reserve(maxTileCount);

	// set the top left tile position
	const sf::Vector2f topLeft = (_gameTypeSelected == GameType::eight) ? sf::Vector2f(80.f, 120.f) : sf::Vector2f(30.f, 120.f);

	// loop through all tiles
	for (auto i = 0u; i < maxTileCount; ++i)
	{
		// get the texture
		if (!_textures[i].loadFromFile(path.c_str() + std::to_string(i) + ".png"))
			throw std::runtime_error("can't load Texture: " + std::to_string(i));

		// get the tile size
		const static auto tileSize = sf::Vector2f(_textures[0].getSize());

		// calculate the tile position on the grid
		sf::Vector2f position(tileSize.x * (i % gridSize), tileSize.y * (i / gridSize));

		// add tile to end of vector
		_tiles.emplace_back(_textures[i]);

		// set the position and index 
		_tiles.back().setPosition(topLeft.x + position.x, topLeft.y + position.y);
		_tiles.back().setIndex(i);

		_tiles[i].initialPosition = position;
	}
}

// shuffles the tiles on the board
void Puzzle::shuffle(GameType::Type gameType)
{
	_win = false;
	// the minimum value in the randomisation
	int min = 1;

	// the maximum value
	int max = (gameType == GameType::eight) ? 8 : 15;

	// generate random
	std::random_device rd;     // only used once to initialise (seed) engine
	std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
	std::uniform_int_distribution<int> uni(min, max); // guaranteed unbiased

	// shuffle the array of tiles
	//std::shuffle(_tiles.begin(), _tiles.end(), rng);
	/*auto temp = _tiles[8];
	_tiles[8] = _tiles[7];
	_tiles[7] = temp;*/

	// example, solvable initial state
	std::vector<Tile> it;
	it.clear();
	if (_currentState != nullptr)
	delete _currentState;
	if (gameType == GameType::fifteen)
	{
		it.push_back(_tiles[1]);
		it.push_back(_tiles[4]);
		it.push_back(_tiles[2]);
		it.push_back(_tiles[3]);
		it.push_back(_tiles[0]);
		it.push_back(_tiles[6]);
		it.push_back(_tiles[9]);
		it.push_back(_tiles[7]);
		it.push_back(_tiles[8]);
		it.push_back(_tiles[5]);
		it.push_back(_tiles[14]);
		it.push_back(_tiles[10]);
		it.push_back(_tiles[15]);
		it.push_back(_tiles[12]);
		it.push_back(_tiles[13]);
		it.push_back(_tiles[11]);
	}
	else if (gameType == GameType::eight)
	{
		it.push_back(_tiles[1]);
		it.push_back(_tiles[5]);
		it.push_back(_tiles[3]);
		it.push_back(_tiles[6]);
		it.push_back(_tiles[0]);
		it.push_back(_tiles[2]);
		it.push_back(_tiles[8]);
		it.push_back(_tiles[4]);
		it.push_back(_tiles[7]);
	}
	_tiles = it;

	// store the current state
	_currentState = new State(_tiles, 0, nullptr);

	shuffled = true;
}

// toggles the currently selected game type
void Puzzle::toggleGameType()
{
	shuffled = false;
	// set the new game type
	if (_gameTypeSelected == GameType::eight)
		_gameTypeSelected = GameType::fifteen;
	else
		_gameTypeSelected = GameType::eight;

	// re-construct the board
	constructBoard();

	// store the inital state (win state)
	_initialState = new State(_tiles, 0, nullptr);

	// shuffle the board
	if (!shuffled)
	shuffle(_gameTypeSelected);
}

// checks to see if the tiles array is sorted in the correct order
// i.e. the puzzle is solved
bool Puzzle::isSolved() const
{
	return std::is_sorted(_tiles.cbegin(), _tiles.cend());
}

/// breadth first search algorithm
void Puzzle::breadthFirstSearch()
{
	// clear all variables
	winPath.clear();
	openStates.empty();
	closedStates.empty();
	_win = false;

	// add initial state to open list
	openStates.push(_currentState);

	// the distance to the next node
	int distance = 1;

	// loop while we have open states
	while (openStates.size() > 0)
	{
		// get the top of the list
		State* state = openStates.front();
		openStates.pop();

		distance = state->_distance + 1; // distance is fixed.

		// compare with initial state
		if (state->Value().compare(_initialState->Value()) == 0) // http://stackoverflow.com/questions/14120346/c-fastest-method-to-check-if-all-array-elements-are-equal
		{
			closedStates.push(state);
			break;
		}
		else
		{
			// get all potential children and add to open list
			std::vector<State*> children = CalculateChildren(state, distance); //the parents are not getting carried over from CalculateChildren
			for (auto const& value : children) {
				openStates.push(value);
			}
			children.clear();
		}
	}

	//add all the states in the closed list to the win list
	State* node = closedStates.front();
	closedStates.pop();
	do
	{
		winPath.push_back(node);
		node = node->_parent;
	} while (node->_distance > 0);

	// add the start state
	winPath.push_back(_currentState);

	// loop through all states(backwards) to get to win
	for (int i = winPath.size() - 1; i >= 0; i--)
	{
		winPath[i]->getTilesbyIndexes(_initialState);
		statesForDisplay.push_back(winPath[i]);
	}
	
	// set win flag
	_win = true;
	shuffled = false;
}

// run the astar algorithm with the desired inital and goal states
void Puzzle::runAstar()
{
	astar(_currentState, _initialState);
}

void Puzzle::astar(State* start, State* goal)
{
	// clear all variables
	winPath.clear();
	openStates.empty();
	closedStates.empty();

	// open list
	std::list<State*> openList;

	// closed list
	std::list<State*> closedList;

	// define working state
	State* current;

	// add start node to open list
	openList.push_back(start);

	// loop while states on open list
	while (openList.size() > 0 && !_win)
	{
		// find the state with lowest f score from open list
		current = findStateWithLowestFScore(openList);

		// remove current from open list
		openList.remove(current);

		// get all the children for current
		std::vector<State*> children = CalculateChildren(current, current->_distance + 1);

		// loop through all children
		for (State* state : children)
		{
			// break from loop if child is goal
			if (std::equal(current->_indexes.begin(), current->_indexes.end(), _initialState->_indexes.begin()))
			{
				_win = true;
				break;
			}

			// set fscore
			state->FScore = state->_distance + heuristicCostEstimate(state);

			// check for state in open list
			auto currentlyInOpenList = std::find(openList.begin(), openList.end(), state);
			if (currentlyInOpenList != openList.end())
			{
				if (((State*)*currentlyInOpenList)->FScore < state->FScore)
				{
					continue;
				}
			}

			// check for state in closed list
			auto currentlyInClosedList = std::find(closedList.begin(), closedList.end(), state);
			if (currentlyInClosedList != closedList.end())
			{
				if (((State*)*currentlyInClosedList)->FScore < state->FScore)
				{
					continue;
				}
			}

			// add state to open list
			openList.push_back(state);
		}

		// add to closed list
		closedList.push_back(current);
	}

	//add all the states in the closed list to the win list
	State* node = closedList.back();
	do
	{
		winPath.push_back(node);
		node = node->_parent;
	} while (node->_distance > 0);

	// add the start state
	winPath.push_back(_currentState);

	statesForDisplay.clear();

	// loop through all states(backwards) to get to win
	for (int i = winPath.size() - 1; i >= 0; i--)
	{
		winPath[i]->getTilesbyIndexes(_initialState);
		statesForDisplay.push_back(winPath[i]);
	}

	// set win flag
	_win = true;
	shuffled = false;
}

// calculate the heuristic for the state using manhattan
int Puzzle::heuristicCostEstimate(State* start)
{
	int estimate = 0;
	//estimate += hamming_distance(start->Value(), _initialState->Value());
	estimate += manhattan(start);
	start->_heuristicValue = estimate;

	return estimate;
}

State* Puzzle::findStateWithLowestFScore(std::list<State*> states)
{
	// the state to return
	State* s = nullptr;

	// create a copy of states
	std::list<State*> statesCopy = states;
	//std::sort(statesCopy.begin(), statesCopy.end(), StateComparer());
	statesCopy.sort(StateComparer());
	s = statesCopy.front();

	// return the state
	return s;
}

// cacluate the manhattan distance
int Puzzle::calculateManhattanDistance(Tile* tile)
{
	// the manhattan distance
	float hValue = 0;

	// check how far away from the target location the tile is
	if (tile->getPosition().x != tile->initialPosition.x)
	{
		// set the heuristic value
		hValue += abs(tile->initialPosition.x - tile->getPosition().x);
		tile->heuristicValue = (int)hValue;
	}
	if (tile->getPosition().y != tile->initialPosition.y)
	{
		// set the heuristic value
		hValue += abs(tile->initialPosition.y - tile->getPosition().y);
		tile->heuristicValue = (int)hValue;
	}

	// if the tiles do match reset the heuristic value
	if (tile->getPosition().x == tile->initialPosition.x && tile->getPosition().y == tile->initialPosition.y)
	{
		tile->heuristicValue = 0;
	}

	// return it
	return (int)hValue;
}

int Puzzle::manhattan(State* start) const
{
	auto count = 0;

	unsigned int gridSize = (_gameTypeSelected == GameType::eight) ? 3u : 4u;

	for (auto it = start->_indexes.cbegin(); it != start->_indexes.cend(); ++it) {
		int i = (it - start->_indexes.cbegin()) / gridSize;
		int j = it - start->_indexes.cbegin() - i * gridSize;
		int goalRow = (*it) / gridSize;
		int goalColumn = *it - goalRow*gridSize;

		count += (abs(goalRow - i) + abs(goalColumn - j));
	}

	return count;
}

// Calculate the potential children for a parent state
std::vector<State*> Puzzle::CalculateChildren(State* parent, int distance)
{
	// list of chidren
	std::vector<State*> children;

	// get the position of the empty tile for the parent state
	int zeroKey = getEmptyTileIndex(parent->_indexes, (_gameTypeSelected == GameType::eight) ? 8 : 15);

	// get the keys for all tiles around the empty tile
	std::vector<int> keys = GetChildKeys(zeroKey);

	// create a new state for each child
	for (unsigned int i = 0; i < keys.size(); i++)
	{
		State* newState = new State(parent->_indexes, _initialState, distance, parent);
		int k = keys[i];
		newState->Swap(k, zeroKey);
		children.push_back(newState);
	}

	// return the children found
	return children;
}

// get the index positions for adjacent tiles in the grid
std::vector<int> Puzzle::GetChildKeys(int zeroKey)
{
	std::vector<int> keys;

	switch (zeroKey)
	{
		case 0:
			keys.push_back(1);
			if (_gameTypeSelected == GameType::eight)
				keys.push_back(3);
			else
				keys.push_back(4);
			break;
		case 1:
			keys.push_back(0);
			keys.push_back(2);
			if (_gameTypeSelected == GameType::eight)
				keys.push_back(4);
			else
				keys.push_back(5);
			break;
		case 2:
			keys.push_back(1);
			if (_gameTypeSelected == GameType::eight)
			{
				keys.push_back(5);
			}
			else
			{
				keys.push_back(6);
				keys.push_back(3);
			}
			break;
		case 3:
			if (_gameTypeSelected == GameType::eight)
			{
				keys.push_back(0);
				keys.push_back(4);
				keys.push_back(6);
			}
			else
			{
				keys.push_back(2);
				keys.push_back(7);
			}
			break;
		case 4:
			if (_gameTypeSelected == GameType::eight)
			{
				keys.push_back(1);
				keys.push_back(3);
				keys.push_back(5);
				keys.push_back(7);
			}
			else
			{
				keys.push_back(0);
				keys.push_back(5);
				keys.push_back(8);
			}
			break;
		case 5:
			if (_gameTypeSelected == GameType::eight)
			{
				keys.push_back(2);
				keys.push_back(4);
				keys.push_back(8);
			}
			else
			{
				keys.push_back(1);
				keys.push_back(4);
				keys.push_back(6);
				keys.push_back(9);
			}
			break;
		case 6:
			if (_gameTypeSelected == GameType::eight)
			{
				keys.push_back(3);
				keys.push_back(7);
			}
			else
			{
				keys.push_back(2);
				keys.push_back(5);
				keys.push_back(10);
				keys.push_back(7);
			}
			break;
		case 7:
			if (_gameTypeSelected == GameType::eight)
			{
				keys.push_back(6);
				keys.push_back(4);
				keys.push_back(8);
			}
			else
			{
				keys.push_back(3);
				keys.push_back(6);
				keys.push_back(11);
			}
			break;
		case 8:
			if (_gameTypeSelected == GameType::eight)
			{
				keys.push_back(5);
				keys.push_back(7);
			}
			else
			{
				keys.push_back(4);
				keys.push_back(9);
				keys.push_back(12);
			}
			break;
		case 9:
			if (_gameTypeSelected != GameType::eight)
			{
				keys.push_back(5);
				keys.push_back(8);
				keys.push_back(10);
				keys.push_back(13);
			}
			break;
		case 10:
			if (_gameTypeSelected != GameType::eight)
			{
				keys.push_back(6);
				keys.push_back(9);
				keys.push_back(11);
				keys.push_back(14);
			}
			break;
		case 11:
			if (_gameTypeSelected != GameType::eight)
			{
				keys.push_back(7);
				keys.push_back(10);
				keys.push_back(15);
			}
			break;
		case 12:
			if (_gameTypeSelected != GameType::eight)
			{
				keys.push_back(8);
				keys.push_back(13);
			}
			break;
		case 13:
			if (_gameTypeSelected != GameType::eight)
			{
				keys.push_back(12);
				keys.push_back(9);
				keys.push_back(14);
			}
			break;
		case 14:
			if (_gameTypeSelected != GameType::eight)
			{
				keys.push_back(10);
				keys.push_back(13);
				keys.push_back(15);
			}
			break;
		case 15:
			if (_gameTypeSelected != GameType::eight)
			{
				keys.push_back(11);
				keys.push_back(14);
			}
			break;
	}

	return keys;
}

// handles key presses
void Puzzle::updateKeyboardEvent(Direction::Type direction)
{
//	unsigned int gridSize = (_gameTypeSelected == GameType::eight) ? 3u : 4u;
//	auto coord = getEmptyTileCoordinates();
//
//	auto x = coord.first.x + directionOffset(direction).x;
//	auto y = coord.first.y + directionOffset(direction).y;
//
//	if (x >= gridSize || y >= gridSize) return;
//
//	std::swap(_tiles[y * gridSize + x], _tiles[coord.second]);
}

// handles mouse events
void Puzzle::updateMouseEvent(sf::Vector2f position)
{
	const static auto bound = std::bind(&Tile::getGlobalBounds, std::placeholders::_1);
	auto lambda = std::bind(contains<>(), bound, position);

	auto found = std::find_if(_tiles.cbegin(), _tiles.cend(), lambda);

	if (found == _tiles.cend()) return;

	move(found->getIndex());
}

// draw the puzzle
void Puzzle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	for (const auto& tile : _tiles)
		target.draw(tile);
}

std::vector<State*> Puzzle::getStatesForDisplay()
{
	return statesForDisplay;
}

// move the tile at the index to the empty tile position
void Puzzle::move(unsigned int index)
{
	unsigned int gridSize = (_gameTypeSelected == GameType::eight) ? 3u : 4u;
	auto coord = getEmptyTileCoordinates(_tiles).first;

	auto tileCoord = sf::Vector2u(index % gridSize, index / gridSize);

	if (tileCoord.y == coord.y)
		horizontalshift(tileCoord, coord.x);

	else if (tileCoord.x == coord.x)
		verticalshift(tileCoord, coord.y);
}

// shift the selected tile horizontally to the empty tile position
void Puzzle::horizontalshift(sf::Vector2u tileCoord, unsigned int coordX)
{
	unsigned int gridSize = (_gameTypeSelected == GameType::eight) ? 3u : 4u;
	auto index = tileCoord.y * gridSize + tileCoord.x;

	if (tileCoord.x < coordX)
	{
		for (auto x = tileCoord.x; x < coordX; ++x)
			std::swap(_tiles[tileCoord.y * gridSize + (x + 1)], _tiles[index]);
	}
	else
	{
		for (auto x = tileCoord.x; x > coordX; --x)
			std::swap(_tiles[tileCoord.y * gridSize + (x - 1)], _tiles[index]);
	}
}

// shift the tile vertically to the empty tile position
void Puzzle::verticalshift(sf::Vector2u tileCoord, unsigned int coordY)
{
	unsigned int gridSize = (_gameTypeSelected == GameType::eight) ? 3u : 4u;
	auto index = tileCoord.y * gridSize + tileCoord.x;

	if (tileCoord.y < coordY)
	{
		for (auto y = tileCoord.y; y < coordY; ++y)
			std::swap(_tiles[(y + 1) * gridSize + tileCoord.x], _tiles[index]);
	}
	else
	{
		for (auto y = tileCoord.y; y > coordY; --y)
			std::swap(_tiles[(y - 1) * gridSize + tileCoord.x], _tiles[index]);
	}
}

// get the coordinates of the empty tile
Puzzle::Pair Puzzle::getEmptyTileCoordinates(std::vector<Tile> tiles) const
{
	unsigned int maxTileCount = (_gameTypeSelected == GameType::eight) ? 9u : 16u;
	unsigned int gridSize = (_gameTypeSelected == GameType::eight) ? 3u : 4u;

	const static auto indexObject = std::bind(&Tile::getIndex, std::placeholders::_1);
	auto lambda = std::bind(std::equal_to<unsigned int>(), indexObject, maxTileCount - 1);

	auto found = std::find_if(tiles.cbegin(), tiles.cend(), lambda);

	assert(found != tiles.cend());

	auto index = std::distance(tiles.cbegin(), found);
	auto coord = sf::Vector2u(index % gridSize, index / gridSize);

	return{ coord, index };
}

// get the index position of the empty tile
int Puzzle::getEmptyTileIndex(std::vector<int> indexes, int item) const
{
	return std::find(indexes.begin(), indexes.end(), item) - indexes.begin();
}