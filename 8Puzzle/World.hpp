#pragma once
#include <SFML/System/NonCopyable.hpp>
#pragma once

#include "Puzzle.hpp"

#include <SFML/Window/Keyboard.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>

#include <map>

namespace sf
{
	class RenderWindow;
	class Event;
}

/*
* definition of the world class
*/
class World : sf::NonCopyable
{
	using KeysMap = std::map<sf::Keyboard::Key, Direction::Type>;

public:
	// constructor
	explicit World(sf::RenderWindow& window);

	// draw the world
	void draw();

	// event handler
	void handleEvent(const sf::Event& event);


private:
	sf::RenderWindow& _window; // the main window

	Puzzle _puzzle; // the puzzle instance

	sf::Sprite _background; // background sprite
	sf::Texture _backgroundTexture; // background texture

	sf::Sprite _shuffleButton; // the shuffle button sprite
	sf::Texture _shuffleButtonTexture; // the shuffle button texture

	sf::Sprite _changeGameTypeButton; // toggle the game type between 8 and 15
	sf::Texture _changeGameTypeButtonTexture; // the texture for the change game type button

	sf::Sprite _breadthFirstButton; // the breadth first button sprite
	sf::Texture _breadthFirstButtonTexture; // the texture the breadth first button sprite

	sf::Sprite _astarButton; // the astar button sprite
	sf::Texture _astarButtonTexture; // the texture for the astar button sprite

	KeysMap _keyBinding; // key binding

	sf::Font _font; // font
	sf::Text _text; // text
};