#pragma warning(disable:4996)
#include "World.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include "stdlib.h"

// Constructor for creating the "world"
World::World(sf::RenderWindow& window)
	: _window(window)
	, _puzzle()
	, _shuffleButtonTexture()
	, _changeGameTypeButtonTexture()
	, _breadthFirstButtonTexture()
	, _astarButtonTexture()
	, _backgroundTexture()
	, _shuffleButton()
	, _changeGameTypeButton()
	, _breadthFirstButton()
	, _astarButton()
	, _background()
	, _keyBinding()
	, _font()
	, _text()
{
	// load the texture files for the main window
	if (!_shuffleButtonTexture.loadFromFile("Media/shuffle.png"))
		throw std::runtime_error("can't load shuffle texture");

	if (!_changeGameTypeButtonTexture.loadFromFile("Media/changeGame.png"))
		throw std::runtime_error("can't load change game texture");

	if (!_breadthFirstButtonTexture.loadFromFile("Media/breadthFirst.png"))
		throw std::runtime_error("can't load breadth First texture");

	if (!_astarButtonTexture.loadFromFile("Media/astar.png"))
		throw std::runtime_error("can't load astar texture");

	if (!_backgroundTexture.loadFromFile("Media/background.png"))
		throw std::runtime_error("can't load background texture");

	// set the textures
	_shuffleButton.setTexture(_shuffleButtonTexture);
	_changeGameTypeButton.setTexture(_changeGameTypeButtonTexture);
	_breadthFirstButton.setTexture(_breadthFirstButtonTexture);
	_astarButton.setTexture(_astarButtonTexture);
	_background.setTexture(_backgroundTexture);

	// set up buttons
	_shuffleButton.setPosition(10, 10);
	_changeGameTypeButton.setPosition(10, 60); 
	_breadthFirstButton.setPosition(300, 10);
	_astarButton.setPosition(300, 60);

	// initialise keys binding
	_keyBinding.emplace(sf::Keyboard::Up, Direction::Up);
	_keyBinding.emplace(sf::Keyboard::Down, Direction::Down);
	_keyBinding.emplace(sf::Keyboard::Left, Direction::Left);
	_keyBinding.emplace(sf::Keyboard::Right, Direction::Right);

	// load the font pack
	if (!_font.loadFromFile("Media/Sansation.ttf"))
		throw std::runtime_error("can't load font");

	_text.setFont(_font);
	_text.setString("Calculating");
	_text.setCharacterSize(70);
	auto bounds(_text.getLocalBounds());
	auto x = std::floor(bounds.left + bounds.width / 2.f);
	auto y = std::floor(bounds.top + bounds.height / 2.f);
	_text.setOrigin(x, y);
	_text.setPosition(_window.getView().getSize() / 2.f);
}

// Draw the "world"
void World::draw()
{
	_window.draw(_background);
	_window.draw(_shuffleButton);
	_window.draw(_changeGameTypeButton);
	_window.draw(_breadthFirstButton);
	_window.draw(_astarButton);
	_window.draw(_puzzle);

	if (_puzzle._win)
	{
		for (State* state : _puzzle.getStatesForDisplay())
		{
			_window.draw(_background);
			_window.draw(_shuffleButton);
			_window.draw(_changeGameTypeButton);
			_window.draw(_breadthFirstButton);
			_window.draw(_astarButton);
			_puzzle._tiles = state->_gameboard;
			_window.draw(_puzzle);
			_window.display();
			_sleep(500);
		}
		_puzzle._win = false;
	}
}

// handle global events
void World::handleEvent(const sf::Event& event)
{
	if (event.type == sf::Event::KeyPressed) // key presses
	{
		auto found = _keyBinding.find(event.key.code);

		if (found != _keyBinding.end())
			_puzzle.updateKeyboardEvent(found->second);
	}
	else if (event.type == sf::Event::MouseButtonPressed) // mouse events
	{
		if (event.mouseButton.button == sf::Mouse::Left)
		{
			auto x = static_cast<float>(event.mouseButton.x);
			auto y = static_cast<float>(event.mouseButton.y);

			// shuffle button pressed
			if (_shuffleButton.getGlobalBounds().contains({ x, y })&& !_puzzle.shuffled)
				_puzzle.shuffle(_puzzle._gameTypeSelected);

			// change game type button pressed
			if (_changeGameTypeButton.getGlobalBounds().contains({ x, y }))
				_puzzle.toggleGameType();

			// breadth first search button pressed
			if (_breadthFirstButton.getGlobalBounds().contains({ x, y }))
			{
				const static auto x = _window.getView().getSize().x;
				const static auto y = _window.getView().getSize().y / 4.f;
				static sf::RectangleShape backgroundShape({ x, y });
				backgroundShape.setFillColor({ 0, 0, 0, 150 });
				backgroundShape.setPosition(0, _window.getSize().y * (3 / 8.f));

				_window.draw(backgroundShape);
				_window.draw(_text);
				_window.display();

				_puzzle.breadthFirstSearch();

				_window.clear();
				_window.display();
			}

			// breadth first search button pressed
			if (_astarButton.getGlobalBounds().contains({ x, y }))
				_puzzle.runAstar();

			_puzzle.updateMouseEvent({ x, y });
		}
	}
}