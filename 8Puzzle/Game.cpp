#include "Game.hpp"
#include <SFML/Window/Event.hpp>

// Constructor to build game window
Game::Game(const std::string& title, unsigned width, unsigned height)
	: _window(sf::VideoMode(width, height), title)
	, _world(_window)
	, _title(title)
	, _fullScreen(false)
{
	_window.setFramerateLimit(60);
}

// the game run sate
void Game::run()
{
	while (_window.isOpen())
	{
		processEvents();
		render();
	}
}

// event handler to process game window events
void Game::processEvents()
{
	const static auto initialSize = _window.getSize();
	const static auto videoMode = sf::VideoMode(initialSize.x, initialSize.y);

	sf::Event event;

	while (_window.pollEvent(event))
	{
		_world.handleEvent(event);

		if (event.type == sf::Event::Closed)
			_window.close();

		else if (event.type == sf::Event::Resized)
		{
			auto width = static_cast<float>(event.size.width);
			auto height = static_cast<float>(event.size.height);

			auto x = initialSize.x / 2.f - width / 2.f;
			auto y = initialSize.y / 2.f - height / 2.f;

			_window.setView(sf::View({ x, y, width, height }));
		}

		else if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Escape)
				_window.close();

			else if (event.key.code == sf::Keyboard::F1)
			{
				_fullScreen = !_fullScreen;

				auto style = _fullScreen ? sf::Style::Fullscreen : sf::Style::Default;

				_window.create(videoMode, _title, style);
			}
		}
	}
}

// render the window to the screen
void Game::render()
{
	_window.clear();
	_world.draw();
	_window.display();
}