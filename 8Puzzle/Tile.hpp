#pragma once

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>


/*
* a game tile
*/
class Tile : public sf::Drawable, public sf::Transformable
{

public:
	// Constructor
	explicit Tile(const sf::Texture& texture);

	// index property
	void setIndex(unsigned int index);
	unsigned int getIndex() const;

	// calulate the bounds of the tile
	sf::FloatRect getGlobalBounds() const;

	// comparison operator
	friend bool operator< (const Tile& lhs, const Tile& rhs);

	// comparison operator
	friend bool operator== (const Tile& lhs, const Tile& rhs);
	
	// used for manhattan
	int heuristicValue;

	// initial tile position
	sf::Vector2f initialPosition;

private:

	// draws the tile
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	sf::Texture _texture; // the texture associated with the tile
	sf::Sprite _sprite; // the sprite associated with the tile

	unsigned int _index; // the index position of the tile
};
