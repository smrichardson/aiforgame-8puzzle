#include "Utility.hpp"

#include <array>
#include <utility>
#include <algorithm>
#include <functional>

sf::Vector2i utility::directionOffset(Direction::Type direction)
{
	using OffsetHolder = std::array<int, Direction::DirectionCount>;

	const static OffsetHolder xOffset{{ 0, 0, 1, -1 }};
	const static OffsetHolder yOffset{{ 1, -1, 0, 0 }};

	return{ xOffset[direction], yOffset[direction] };
}