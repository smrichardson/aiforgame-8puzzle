#pragma once

/*
* The direction the tile moves
*/
namespace Direction
{
	enum Type
	{
		Up,
		Down,
		Left,
		Right,
		DirectionCount
	};
}