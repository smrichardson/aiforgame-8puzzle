#pragma once
#include "Tile.hpp"
#include "BaseTile.hpp"

class State
{
public:
	// default constructor
	State();

	// constructor
	State(std::vector<int> board, int distance, State* parent);

	// constructor
	State(std::vector<Tile> board, int distance, State* parent);

	// constructor
	State(std::vector<int> board, State* initial, int distance, State* parent);

	// constructor
	State(std::vector<Tile> board, State* initial, int distance, State* parent);

	// destructor
	~State();

	// value representation for comparison
	std::string Value();

	// get the tiles related to this state
	std::vector<Tile> getTilesbyIndexes(State* initial);

	// swap two elements
	void Swap(int i, int j);

	State* _parent; // the parent of this state

	std::vector<Tile> _gameboard; // the gameboard
	std::vector<int> _indexes;

	int FScore;

	int _distance; // the distance from previous state

	int _heuristicValue; // heuristic used for manhattan
};

class StateComparer {
public:
	bool operator() (const State *lhs, const State *rhs) {
		return (*lhs).FScore < (*rhs).FScore;
	}
};