#pragma once

#include "Direction.hpp"
#include "Tile.hpp"
#include <SFML/System/Vector2.hpp>

#include <random>


namespace utility
{
	template<typename T = sf::FloatRect>
	struct contains;

	template<typename T = sf::FloatRect>
	struct intersects;

	sf::Vector2i directionOffset(Direction::Type direction);
}

#include "Utility.inl"