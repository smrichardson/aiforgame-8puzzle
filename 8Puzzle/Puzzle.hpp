#pragma once

#include <SFML/System/NonCopyable.hpp>
#include "Tile.hpp"
#include "BaseTile.hpp"
#include "Direction.hpp"
#include "GameType.hpp"
#include "State.hpp"
#include <array>
#include <queue>
#include <random>
#include <set>
#include <list>

/*
* represents the puzzle object
*/
class Puzzle : public sf::Drawable, sf::NonCopyable
{

private:
	const static int MaxTiles = 16u; // max number of tiles on the board
	const static int Size = 4u; // number of tile rows/columns

	using Pair = std::pair<sf::Vector2u, unsigned int>;
	using Textureholder = std::array<sf::Texture, MaxTiles>; // construct to hold all textures
	using TileHolder = std::vector<Tile>; // construct to hold all tile objects

public:
	
	// Constructor
	Puzzle();

	// Destructor
	~Puzzle();

	// Shuffle the tiles
	void shuffle(GameType::Type gameType);

	// breadth first search solution
	void breadthFirstSearch();

	// astar solution
	void astar(State* start, State* goal);

	// calculate manhattan distance
	int calculateManhattanDistance(Tile* tile);

	int manhattan(State* start) const;

	// the overall heuristic estimate
	int heuristicCostEstimate(State* start);

	// toggles the game type between 8 and 15
	void toggleGameType();

	// Check is puzzle is solved
	bool isSolved() const;

	// handlers for keyboard and mouse
	void updateKeyboardEvent(Direction::Type direction);
	void updateMouseEvent(sf::Vector2f position);

	bool _win; // the win flag

	std::vector<State*> getStatesForDisplay();
	TileHolder _tiles; // puzzle tiles

	//std::set<State*> sortByFscore(std::set<State*> states);

	State* findStateWithLowestFScore(std::list<State*> states);

	void runAstar();

	bool shuffled = false;
	GameType::Type _gameTypeSelected; // the type of game selected, 8 or 1
private:

	// construct the board
	void constructBoard();

	// draw the puzzle
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	// move a tile
	void move(unsigned int index);

	// shift tiles horizontally
	void horizontalshift(sf::Vector2u tileCoord, unsigned int coordX);

	// shift tiles vertically
	void verticalshift(sf::Vector2u tileCoord, unsigned int coordY);

	// get the empty tile position
	Pair getEmptyTileCoordinates(std::vector<Tile> tiles) const;
	int getEmptyTileIndex(std::vector<int> indexes, int item) const;

	// calcuate children for a specific state
	std::vector<State*> CalculateChildren(State* parent, int distance);

	// get the Tiles next to the empty tile
	std::vector<int> GetChildKeys(int zeroKey);
	
private:

	Textureholder _textures; // puzzle textures
	
	

	// breadth first search constructs
	State* _currentState;
	State* _initialState;
	std::queue<State*> closedStates;
	std::queue<State*> openStates;
	std::vector<State*> statesForDisplay;
	std::vector<State*> winPath;
};