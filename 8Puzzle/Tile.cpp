#include "Tile.hpp"

#include <SFML/Graphics/RenderTarget.hpp>


Tile::Tile(const sf::Texture& texture)
	: _texture(texture)
	, _sprite(_texture)
	, _index()
{
}

sf::FloatRect Tile::getGlobalBounds() const
{
	return getTransform().transformRect(_sprite.getLocalBounds());
}

void Tile::setIndex(unsigned int index)
{
	_index = index;
}

unsigned int Tile::getIndex() const
{
	return _index;
}

void Tile::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform = getTransform();
	target.draw(_sprite, states);
}

bool operator< (const Tile& lhs, const Tile& rhs)
{
	return lhs._index < rhs._index;
}

bool operator==(const Tile& lhs, const Tile& rhs)
{
	return lhs.getIndex() == rhs.getIndex();
}


