#include "BaseTile.hpp"

#include <SFML/Graphics/RenderTarget.hpp>


BaseTile::BaseTile() : _index()
{
}

void BaseTile::setIndex(unsigned int index)
{
	_index = index;
}

unsigned int BaseTile::getIndex() const
{
	return _index;
}

//bool operator< (const BaseTile& lhs, const BaseTile& rhs)
//{
//	return lhs._index < rhs._index;
//}
//
//bool operator==(const BaseTile& lhs, const BaseTile& rhs)
//{
//	return lhs.getIndex() == rhs.getIndex();
//}


