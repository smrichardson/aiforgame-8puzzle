#pragma once

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>

class BaseTile : public sf::Transformable
{
public:

	// Constructor
	BaseTile();

	// destructor
	~BaseTile();

	// index property
	void setIndex(unsigned int index);
	unsigned int getIndex() const;

	//// comparison operator
	//friend bool operator< (const BaseTile& lhs, const BaseTile& rhs);

	//// comparison operator
	//friend bool operator== (const BaseTile& lhs, const BaseTile& rhs);
	
	// used for manhattan
	int heuristicValue;

	// initial tile position
	sf::Vector2f initialPosition;

protected:

	unsigned int _index; // the index position of the tile
};