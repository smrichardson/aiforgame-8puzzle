template<typename T>
struct utility::contains
{
	bool operator()(const T& lhs, const sf::Vector2f& rhs) const
	{
		return lhs.contains(rhs);
	}
};

template<typename T>
struct utility::intersects
{
	bool operator()(const T& lhs, const T& rhs) const
	{
		return lhs.intersects(rhs);
	}
};