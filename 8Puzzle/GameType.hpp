#pragma once

/*
* The game type 8 or 15
*/
namespace GameType
{
	enum Type
	{
		eight,
		fifteen
	};
}